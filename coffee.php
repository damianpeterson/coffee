<?php

class Coffee
{
    private $people;

    public function __construct()
    {
        $this->people = $this->getPeopleFromFile();
    }

    /**
     * Read the contents of the ini file into an array
     *
     * @return array
     */
    public function getPeopleFromFile()
    {
        $people = [];
        $data = file_get_contents(__DIR__ . '/coffee.ini');

        if ($data === false) {
            return ['name' => 'error', 'points' => 0];
        }

        $data = explode(PHP_EOL, $data);

        foreach ($data as $key => $value) {
            $person = explode('=', $value);
            if ($person[0]) {
                $people[] = ['name' => $person[0], 'points' => $person[1]];
            }
        }

        return $people;
    }

    public function getPeople()
    {
        return $this->people;
    }

    /**
     * Take post data and find givers and receivers to update their points
     *
     * @param $data
     * @return int
     */
    public function addTransaction($data)
    {
        // make sure all givers and receivers are valid
        $giver = null;
        $receivers = [];

        foreach ($data as $person) {
            if ($person['isGiver'] === '1') {
                $giver = $person['name'];
            }
            if ($person['isReceiver'] === '1') {
                $receivers[] = $person['name'];
            }
        }

        if (!$giver || empty($receivers)) {
            return 0;
        }

        // update points
        $this->updatePersonPoints($giver, count($receivers));

        foreach ($receivers as $receiver) {
            $this->updatePersonPoints($receiver, -1);
        }

        // save over existing data
        $result = file_put_contents(__DIR__ . '/coffee.ini', $this->getPeopleAsString());

        // log transaction
        error_log(sprintf('[%s] [add] %s'.PHP_EOL, date(DATE_ISO8601), json_encode($data)), 3, __DIR__ . '/transactions.log');

        return $result;
    }

    /**
     * This does the opposite of addTransaction with exactly the same data
     * @param $data
     * @return int
     */
    public function rollBack($data)
    {
        // make sure all givers and receivers are valid
        $givers = [];
        $receiver = null;

        foreach ($data as $person) {
            if ($person['isGiver'] === '1') {
                $receiver = $person['name'];
            }
            if ($person['isReceiver'] === '1') {
                $givers[] = $person['name'];
            }
        }

        if (!$receiver || empty($givers)) {
            return 0;
        }

        // update points
        $this->updatePersonPoints($receiver, 0 - count($givers));

        foreach ($givers as $giver) {
            $this->updatePersonPoints($giver, 1);
        }

        // save over existing data
        $result = file_put_contents(__DIR__ . '/coffee.ini', $this->getPeopleAsString());

        // log transaction
        error_log(sprintf('[%s] [rollback] %s'.PHP_EOL, date(DATE_ISO8601), json_encode($data)), 3, __DIR__ . '/transactions.log');

        return $result;
    }

    /**
     * Find a person and adjust their points
     *
     * @param $name
     * @param $points
     */
    public function updatePersonPoints($name, $points)
    {
        foreach ($this->people as $key => $person) {
            if ($person['name'] === $name) {
                $this->people[$key]['points'] += $points;
                break;
            }
        }
    }

    /**
     * Rebuilds the list of people in ini file format
     * @return string
     */
    public function getPeopleAsString()
    {
        $string = '';
        foreach ($this->getPeople() as $person) {
            $string .= sprintf('%s=%s' . PHP_EOL, $person['name'], $person['points']);
        }

        return $string;
    }
}

$coffee = new Coffee();

/**
 * Interpret the request actions and pass them off to the appropriate methods
 */
$action = $_REQUEST['action'];
if ($action) {
    switch ($action) {
        case 'get_people':
            print json_encode($coffee->getPeople());
            break;
        case 'add_transaction':
            $result = $coffee->addTransaction($_REQUEST['data']);
            print $result;
            break;
        case 'rollback':
            $result = $coffee->rollBack($_REQUEST['data']);
            print $result;
            break;
    }
}
