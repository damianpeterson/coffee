# coffee
A simple web app for keeping track of who should buy the next coffee

If you want to use this you'll need to supply a coffee.ini file with write permissions i.e.
```
damian=0
billy=0
etc=0
```

Also, add a writable transactions.log file to keep a track of transactions and rollbacks.
